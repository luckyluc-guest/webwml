#use wml::debian::template title="Páginas do Debian em Português" NOHEADER="yes"
#use wml::debian::translation-check translation="43643ef97119a325bc202ce3fe9a6883b85505d7" maintainer="Paulo Henrique de Lima Santana (phls)"

# The contents of this page is completely the responsibility of
# the translation team

<h1>Debian em Português</h1>

<p>Esta página traz informações específicas sobre o esforço
de tradução do Debian para o Português. A equipe que está empenhada nas
traduções é formada por brasileiros e, por isso, há um grande predomínio
de português brasileiro nas páginas. Essa equipe, que é composta por
desenvolvedores e usuários do Debian, intitula-se
<a href="https://wiki.debian.org/Brasil">Debian Brasil</a>.</p>

<p>Para saber como se inscrever nas listas de discussão para usuários
falantes do português, bem como instruções de como entrar no canal IRC
do projeto, visite as seções
<a href="https://wiki.debian.org/Brasil/Listas">Listas</a> e
<a href="https://wiki.debian.org/Brasil/IRC">IRC</a>
da página do Debian-BR.</p>

<p>Para ter acesso à documentação em português, seja de manuais
traduzidos ou feitos pelos próprios membros da equipe, visite a
seção <a href="https://wiki.debian.org/Brasil/Documentos">
Documentos</a> da página do Debian Brasil.</p>

<p>Para ver as últimas notícias sobre o mundo Debian, em português, visite
a seção <a href="$(HOME)/News/weekly">Novidades</a> da página do Debian.</p>

<p>Para saber informações sobre os grupos de usuários Debian existentes
no Brasil e as atividades que os mesmos têm realizado, visite a seção
<a href="https://wiki.debian.org/Brasil/GUD">Grupos de Usuários Debian</a>.</p>

<p>Se você <strong>quer</strong> ajudar a desenvolver ainda mais o sistema
Debian, veja algumas das possíveis formas na seção
<a href="https://wiki.debian.org/Brasil/ComoColaborar">Como Colaborar</a>
ou envie um <a href="mailto:debian-br-geral@alioth-lists.debian.net">e-mail</a>.</p>

<p>Apesar de o nome do projeto ser "Brasil", temos vários amigos portugueses
nas listas de discussão e em nosso canal de IRC, serão muito bem-vindos
aqueles que quiserem se juntar a nós.</p>

<p>Obrigado!</p>

<p>Equipe do Debian Brasil &lt;<a href="mailto:debian-br-geral@alioth-lists.debian.net">debian-br-geral@alioth-lists.debian.net</a>&gt;
</p>
