#use wml::debian::translation-check translation="165625c3a669960bb5e1c766db812564b5fd665e" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i implementationen av
programspråket Perl. Projektet Common Vulnerabilities and Exposures
identifierar följande problem:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18311">CVE-2018-18311</a>

	<p>Jayakrishna Menon och Christophe Hauser upptäckte ett
	heltalsspill i Perl_my_setenv som leder till ett heap-baserat
	buffertspill med angriparkontrollerad indata.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18312">CVE-2018-18312</a>

	<p>Eiichi Tsukata upptäckte att ett skapat reguljärt uttryck kunde
	orsaka en heapbaserad buffertspillskrivning under kompilering,
	vilket potentiellt tillåter exekvering av godtycklig kod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18313">CVE-2018-18313</a>

	<p>Eiichi Tsukata upptäckte att ett skapat reguljärt uttryck kunde
	orsaka en heapbaserad buffertspillsläsning under kompilering vilket
	leder till informationsläckage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18314">CVE-2018-18314</a>

	<p>Jakub Wilk upptäckt att ett speciellt skapat reguljärt uttryck
	kunde leda till ett heap-baserat buffertspill.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 5.24.1-3+deb9u5.</p>

<p>Vi rekommenderar att ni uppgraderar era perl-paket.</p>

<p>För detaljerad säkerhetsstatus om perl vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/perl">https://security-tracker.debian.org/tracker/perl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4347.data"
