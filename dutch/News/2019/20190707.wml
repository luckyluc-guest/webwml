<define-tag pagetitle>Debian Edu / Skolelinux Buster — een volledige Linuxoplossing voor uw school</define-tag>
<define-tag release_date>2019-07-07</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="df8b497185c1a26f20b79e08fe7543e9c4d55353" maintainer="Frans Spiesschaert"

<p>
Moet u een computerlabo of een volledig schoolnetwerk beheren? Wilt u servers, werkstations en laptops installeren die onderling samenwerken? Wenst u de stabiliteit van Debian met reeds vooraf geconfigureerde netwerkdiensten? Wilt u systemen en verschillende honderden of zelfs meer gebruikersaccounts beheren met een webtoepassing? Heeft u zich al afgevraagd of en hoe u oudere computers kunt blijven gebruiken?
</p>

<p>
Dan is Debian Edu iets voor u. Leerkrachten zelf of de technische dienst van de school kunnen op enkele dagen tijd een volledige leeromgeving ontplooien voor veel gebruikers en met veel computers. Debian Edu wordt geleverd met honderden vooraf geïnstalleerde toepassingen, maar u kunt er steeds nog andere pakketten uit Debian aan toevoegen.
</p>

<p>
Het team van ontwikkelaars van Debian Edu is verheugd Debian Edu 10
<q>Buster</q> te kunnen aankondigen, de uitgave van Debian Edu / Skolelinux die gebaseerd is op de uitgave van Debian 10 <q>Buster</q>.
Misschien kunt u deze uitgave uittesten en ons uw bevindingen laten weten (&lt;debian-edu@lists.debian.org&gt;) om ons zo te helpen ze nog verder te verbeteren.
</p>

<h2>Over Debian Edu en Skolelinux</h2>

<p>
<a href="http://www.skolelinux.org/">Debian Edu, ook bekend als Skolelinux</a>, is een op Debian gebaseerde Linux-distributie met een gebruiksklaar en volledig vooraf geconfigureerd schoolnetwerk. Onmiddellijk na installatie wordt een schoolserver opgezet waarop alle diensten actief zijn die men in een schoolnetwerk nodig heeft. Er moeten dan nog enkel gebruikers en computers in het systeem ingevoerd worden via GOsa², een comfortabele webinterface. Er staat een omgeving ter beschikking om computers over het netwerk op te starten, waardoor na de initiële installatie van de centrale server via een cd/dvd/BD of USB-stick, alle andere computers over het netwerk geïnstalleerd kunnen worden. Oudere computers (zelfs tot ongeveer tien jaar oud) kunnen gebruikt worden als LTSP thin clients of schijfloze werkstations, die over het netwerk opstarten en geen enkele installatie of configuratie behoeven. De schoolserver van Debian Edu gebruikt een LDAP databank en een Kerberos authenticatiedienst, heeft gecentraliseerde persoonlijke mappen, een DHCP-server, webproxy en vele andere services. De grafische werkomgeving bevat meer dan 60 pakketten met educatieve software en in het Debian-archief zijn er nog meer te vinden. Scholen kunnen kiezen uit de grafische werkomgevingen  Xfce, GNOME, LXDE, MATE, KDE Plasma en LXQt.
</p>

<h2>Nieuwe functionaliteit in Debian Edu 10 <q>Buster</q></h2>

<p>Hierna volgen enkele elementen uit de aantekeningen bij de uitgave van Debian Edu 10 <q>Buster</q>, die gebaseerd is op de uitgave van Debian 10 <q>Buster</q>.
Een volledige lijst met meer gedetailleerde informatie is opgenomen in het betreffende <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">hoofdstuk van de Handleiding bij Debian Edu</a>, die ook <a href="https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-nl/debian-edu-buster-manual.html">in het Nederlands</a> beschikbaar is.
</p>

<ul>
<li>
Er zijn nu officiële Debian installatie-images beschikbaar.
</li>
<li>
Een site-specifieke installatie is mogelijk.
</li>
<li>
De educatieve pakketten werden in extra meta-pakketten gegroepeerd per onderwijsniveau.
</li>
<li>
Een verbeterde taalondersteuning voor de grafische werkomgeving en dit voor alle talen die door Debian ondersteund worden.
</li>
<li>
Er is een hulpprogramma beschikbaar dat het instellen van site-specifieke meertalige ondersteuning vergemakkelijkt.
</li>
<li>
Een GOsa²-plug-in voor wachtwoordbeheer (Password Management) werd toegevoegd.
</li>
<li>
Verbeterde ondersteuning voor TLS/SSL in het interne netwerk.
</li>
<li>
De Kerberos-instellingen ondersteunen de diensten NFS en SSH.
</li>
<li>
Er is een hulpprogramma beschikbaar voor het opnieuw genereren van de LDAP-databank.
</li>
<li>
X2Go-server wordt geïnstalleerd op alle systemen met het profiel LTSP-Server.
</li>
</ul>

<h2>Downloadmogelijkheden, installatiestappen en handleiding</h2>

<p>
Aparte netwerkinstallatie cd-images voor 64-bits en 32-bits pc's zijn beschikbaar. Slechts in uitzonderlijke situaties (voor pc's die ouder zijn dan 12 jaar) zal het 32-bits image nodig zijn. De images kunnen gedownload worden van de volgende locaties:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Er zijn eveneens omvangrijke (meer dan 5 GB) BD-image beschikbaar. Het is mogelijk om een volledig Debian Edu netwerk in te stellen zonder toegang tot internet (alle ondersteunde grafische werkomgevingen, alle door Debian ondersteunde talen). Deze images kunnen gedownload worden van de volgende locaties:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
De images kunnen geverifieerd worden met de ondertekende checksums die in de downloadmap ter beschikking gesteld worden.
<br />
Na het downloaden van een image, kunt u controleren
</p>

<ul>
<li>
of de checksum ervan overeenkomt met de volgens het checksumbestand verwachte checksum;
</li>
<li>
en of er niet geknoeid werd met het checksumbestand.
</li>
</ul>

<p>
Meer informatie over het uitvoeren van deze controles vindt u in de
<a href="https://www.debian.org/CD/verify">verification guide</a>.
</p>

<p>
Debian Edu 10 <q>Buster</q> is volledig gebaseerd op Debian 10 <q>Buster</q>. Bijgevolg is de broncode van alle pakketten beschikbaar in het archief van Debian.
</p>

<p>
Raadpleeg ook de
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">Debian Edu Buster statuspagina</a> voor up-to-date informatie over Debian Edu 10 <q>Buster</q> en voor instructies voor het gebruik van <code>rsync</code> om de ISO-images te downloaden.
</p>

<p>
Als u opwaardeert van Debian Edu 9 <q>Stretch</q>, lees dan het betreffende
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">hoofdstuk uit de handleiding voor Debian Edu.</a>
</p>

<p>
Lees voor installatie-informatie het betreffende
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">hoofdstuk uit de handleiding voor Debian Edu.</a>
</p>

<p>
Na installatie moet u deze
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">eerste maatregelen</a> nemen.
</p>

<p>
Raadpleeg de<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">Debian Edu wiki pagina's</a> voor de recentste Engelstalige versie van de handleiding voor Debian Edu <q>Buster</q>.
De handleiding werd volledig vertaald naar het Duits, het Frans, het Italiaans, het Deens, het Nederlands, het Noorse Bokmål en het Japans. Voor het Spaans en het Vereenvoudigd Chinees bestaat een gedeeltelijke vertaling.
Er bestaat een overzicht van <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
de recentste vertaalde versies van de handleiding</a>.
</p>

<p>
Meer informatie over Debian 10 <q>Buster</q> zelf wordt gegeven in de notities bij de release en de installatiehandleiding; zie <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Over Debian</h2>

<p>Het Debian Project is een samenwerkingsverband van ontwikkelaars van Vrije Software die op vrijwillige basis tijd en energie investeren in het vervaardigen van het volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Raadpleeg voor bijkomende informatie de Debian webpagina's op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail (in het Engels) naar &lt;press@debian.org&gt;.</p>
