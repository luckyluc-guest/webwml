Hola,

Recibe este correo porque mantiene una o más páginas traducidas al español
del sitio web de Debian. La configuración actual de las notificaciones es:

  summary: #$translators{default}{frequency}[$translators{$name}{summary}]#
  logs: #$translators{default}{frequency}[$translators{$name}{logs}]#
  diff: #$translators{default}{frequency}[$translators{$name}{diff}]#
  tdiff: #$translators{default}{frequency}[$translators{$name}{tdiff}]#
  file: #$translators{default}{frequency}[$translators{$name}{file}]#

Puede cambiar la frecuencia de estos mensajes,
la información que se envía por correo o la dirección de correo editando
el archivo webwml/spanish/international/spanish/translator.db.pl.

Si tiene dudas sobre este mensaje, por favor dirija sus preguntas
a la lista debian-l10n-spanish@lists.debian.org.
