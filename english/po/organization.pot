msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr ""

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
msgid "chair"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr ""

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:77
msgid "Officers"
msgstr ""

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:101
msgid "Distribution"
msgstr ""

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:237
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:240
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:244
msgid "Publicity team"
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr ""

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:73
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:80
msgid "Leader"
msgstr ""

#: ../../english/intro/organization.data:82
msgid "Technical Committee"
msgstr ""

#: ../../english/intro/organization.data:96
msgid "Secretary"
msgstr ""

#: ../../english/intro/organization.data:104
msgid "Development Projects"
msgstr ""

#: ../../english/intro/organization.data:105
msgid "FTP Archives"
msgstr ""

#: ../../english/intro/organization.data:107
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:118
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:124
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:128
msgid "Release Management"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Release Team"
msgstr ""

#: ../../english/intro/organization.data:143
msgid "Quality Assurance"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Installation System Team"
msgstr ""

#: ../../english/intro/organization.data:145
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:146
msgid "Release Notes"
msgstr ""

#: ../../english/intro/organization.data:148
msgid "CD Images"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Production"
msgstr ""

#: ../../english/intro/organization.data:158
msgid "Testing"
msgstr ""

#: ../../english/intro/organization.data:160
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:164
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:166
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:173
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:191
msgid "Documentation"
msgstr ""

#: ../../english/intro/organization.data:196
msgid "Work-Needing and Prospective Packages list"
msgstr ""

#: ../../english/intro/organization.data:198
msgid "Ports"
msgstr ""

#: ../../english/intro/organization.data:228
msgid "Special Configurations"
msgstr ""

#: ../../english/intro/organization.data:230
msgid "Laptops"
msgstr ""

#: ../../english/intro/organization.data:231
msgid "Firewalls"
msgstr ""

#: ../../english/intro/organization.data:232
msgid "Embedded systems"
msgstr ""

#: ../../english/intro/organization.data:247
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:249
msgid "Web Pages"
msgstr ""

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr ""

#: ../../english/intro/organization.data:289
msgid "DebConf Committee"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr ""

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "OASIS: Organization\n      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid "OVAL: Open Vulnerability\n      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr ""

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr ""

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr ""

#: ../../english/intro/organization.data:432
msgid "To send a private message to all DAMs, use the GPG key 57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr ""

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr ""

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr ""

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr ""

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr ""

#: ../../english/intro/organization.data:460
msgid "This is the address to use when encountering problems on one of Debian's machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:469
msgid "If you have hardware problems with Debian machines, please see <a href=\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr ""

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr ""

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:487
msgid "<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:490
msgid "Salsa administrators"
msgstr ""

#: ../../english/intro/organization.data:501
msgid "Debian for children from 1 to 99"
msgstr ""

#: ../../english/intro/organization.data:504
msgid "Debian for medical practice and research"
msgstr ""

#: ../../english/intro/organization.data:507
msgid "Debian for education"
msgstr ""

#: ../../english/intro/organization.data:512
msgid "Debian in legal offices"
msgstr ""

#: ../../english/intro/organization.data:516
msgid "Debian for people with disabilities"
msgstr ""

#: ../../english/intro/organization.data:520
msgid "Debian for science and related research"
msgstr ""

#: ../../english/intro/organization.data:523
msgid "Debian for astronomy"
msgstr ""

