<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5419">CVE-2016-5419</a>

    <p>Bru Rom discovered that libcurl would attempt to resume a TLS
    session even if the client certificate had changed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5420">CVE-2016-5420</a>

    <p>It was discovered that libcurl did not consider client certificates
    when reusing TLS connections.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.26.0-1+wheezy14.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-586.data"
# $Id: $
