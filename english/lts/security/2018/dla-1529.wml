<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3620">CVE-2018-3620</a>

    <p>Multiple researchers have discovered a vulnerability in the way
    the Intel processor designs have implemented speculative execution
    of instructions in combination with handling of page-faults. This
    flaw could allow an attacker controlling an unprivileged process
    to read memory from arbitrary (non-user controlled) addresses,
    including from the kernel and all other processes running on the
    system or cross guest/host boundaries to read host memory.</p>

    <p>This issue covers only attackers running normal processes. A
    related issue (<a href="https://security-tracker.debian.org/tracker/CVE-2018-3646">CVE-2018-3646</a>) 
    exists with KVM guests, and is not yet fixed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>

    <p>Multiple researchers have discovered that Speculative Store Bypass
    (SSB), a feature implemented in many processors, could be used to
    read sensitive information from another context.  In particular,
    code in a software sandbox may be able to read sensitive
    information from outside the sandbox.  This issue is also known as
    Spectre variant 4.</p>

    <p>This update allows the issue to be mitigated on some x86
    processors by disabling SSB.  This requires an update to the
    processor's microcode, which is non-free.  DLA 1446-1 and DLA
    1506-1 provided this for some Intel processors.  For other
    processors, it may be included in an update to the system BIOS or
    UEFI firmware, or in a future update to the intel-microcode or
    amd64-microcode packages.</p>

    <p>Disabling SSB can reduce performance significantly, so by default
    it is only done in tasks that use the seccomp feature.
    Applications that require this mitigation should request it
    explicitly through the prctl() system call.  Users can control
    where the mitigation is enabled with the spec_store_bypass_disable
    kernel parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a>

    <p>(FragmentSmack)</p>

    <p>Juha-Matti Tilli discovered a flaw in the way the Linux kernel
    handled reassembly of fragmented IPv4 and IPv6 packets. A remote
    attacker can take advantage of this flaw to trigger time and
    calculation expensive fragment reassembly algorithms by sending
    specially crafted packets, leading to remote denial of service.</p>

    <p>This is mitigated by reducing the default limits on memory usage
    for incomplete fragmented packets. The same mitigation can be
    achieved without the need to reboot, by setting the sysctls:</p>

    <p>net.ipv4.ipfrag_low_thresh = 196608<br>
    net.ipv6.ip6frag_low_thresh = 196608<br>
    net.ipv4.ipfrag_high_thresh = 262144<br>
    net.ipv6.ip6frag_high_thresh = 262144</p>

    <p>The default values may still be increased by local configuration
    if necessary.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6554">CVE-2018-6554</a>

    <p>A memory leak in the irda_bind function in the irda subsystem was
    discovered. A local user can take advantage of this flaw to cause a
    denial of service (memory consumption).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6555">CVE-2018-6555</a>

    <p>A flaw was discovered in the irda_setsockopt function in the irda
    subsystem, allowing a local user to cause a denial of service
    (use-after-free and system crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7755">CVE-2018-7755</a>

    <p>Brian Belleville discovered a flaw in the fd_locked_ioctl function
    in the floppy driver in the Linux kernel. The floppy driver copies a
    kernel pointer to user memory in response to the FDGETPRM ioctl. A
    local user with access to a floppy drive device can take advantage
    of this flaw to discover the location kernel code and data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9363">CVE-2018-9363</a>

    <p>It was discovered that the Bluetooth HIDP implementation did not
    correctly check the length of received report messages. A paired
    HIDP device could use this to cause a buffer overflow, leading to
    denial of service (memory corruption or crash) or potentially
    remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9516">CVE-2018-9516</a>

    <p>It was discovered that the HID events interface in debugfs did not
    correctly limit the length of copies to user buffers.  A local
    user with access to these files could use this to cause a
    denial of service (memory corruption or crash) or possibly for
    privilege escalation.  However, by default debugfs is only
    accessible by the root user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10021">CVE-2018-10021</a>

    <p>A physically present attacker who unplugs a SAS cable can cause a
    denial of service (memory leak and WARN).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10323">CVE-2018-10323</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-13093">CVE-2018-13093</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-13094">CVE-2018-13094</a>

    <p>Wen Xu from SSLab at Gatech reported several NULL pointer
    dereference flaws that may be triggered when mounting and
    operating a crafted XFS volume.  An attacker able to mount
    arbitrary XFS volumes could use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10876">CVE-2018-10876</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10877">CVE-2018-10877</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10878">CVE-2018-10878</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10879">CVE-2018-10879</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10880">CVE-2018-10880</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10881">CVE-2018-10881</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10882">CVE-2018-10882</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10883">CVE-2018-10883</a>

    <p>Wen Xu from SSLab at Gatech reported that crafted ext4 volumes
    could trigger a crash or memory corruption.  An attacker able to
    mount arbitrary ext4 volumes could use this for denial of service
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10902">CVE-2018-10902</a>

    <p>It was discovered that the rawmidi kernel driver does not protect
    against concurrent access which leads to a double-realloc (double
    free) flaw. A local attacker can take advantage of this issue for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13405">CVE-2018-13405</a>

    <p>Jann Horn discovered that the inode_init_owner function in
    fs/inode.c in the Linux kernel allows local users to create files
    with an unintended group ownership allowing attackers to escalate
    privileges by making a plain file executable and SGID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13406">CVE-2018-13406</a>

    <p>Dr Silvio Cesare of InfoSect reported a potential integer overflow
    in the uvesafb driver.  A local user with permission to access
    such a device might be able to use this for denial of service or
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14609">CVE-2018-14609</a>

    <p>Wen Xu from SSLab at Gatech reported a potential null pointer
    dereference in the F2FS implementation. An attacker able to mount
    arbitrary F2FS volumes could use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14617">CVE-2018-14617</a>

    <p>Wen Xu from SSLab at Gatech reported a potential null pointer
    dereference in the HFS+ implementation. An attacker able to mount
    arbitrary HFS+ volumes could use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14633">CVE-2018-14633</a>

    <p>Vincent Pelletier discovered a stack-based buffer overflow flaw in
    the chap_server_compute_md5() function in the iSCSI target code. An
    unauthenticated remote attacker can take advantage of this flaw to
    cause a denial of service or possibly to get a non-authorized access
    to data exported by an iSCSI target.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14634">CVE-2018-14634</a>

    <p>Qualys reported an integer overflow in the initialisation of the
    stack for ELF executables, which can cause the stack to overlap
    the argument or environment strings. A local user may use this to
    defeat environment variable filtering in setuid programs, leading
    to privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14678">CVE-2018-14678</a>

    <p>M. Vefa Bicakci and Andy Lutomirski discovered a flaw in the
    kernel exit code used on amd64 systems running as Xen PV guests.
    A local user could use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14734">CVE-2018-14734</a>

    <p>A use-after-free bug was discovered in the InfiniBand
    communication manager. A local user could use this to cause a
    denial of service (crash or memory corruption) or possible for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15572">CVE-2018-15572</a>

    <p>Esmaiel Mohammadian Koruyeh, Khaled Khasawneh, Chengyu Song, and
    Nael Abu-Ghazaleh, from University of California, Riverside,
    reported a variant of Spectre variant 2, dubbed SpectreRSB. A
    local user may be able to use this to read sensitive information
    from processes owned by other users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15594">CVE-2018-15594</a>

    <p>Nadav Amit reported that some indirect function calls used in
    paravirtualised guests were vulnerable to Spectre variant 2.  A
    local user may be able to use this to read sensitive information
    from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16276">CVE-2018-16276</a>

    <p>Jann Horn discovered that the yurex driver did not correctly limit
    the length of copies to user buffers.  A local user with access to
    a yurex device node could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16658">CVE-2018-16658</a>

    <p>It was discovered that the cdrom driver does not correctly
    validate the parameter to the CDROM_DRIVE_STATUS ioctl.  A user
    with access to a cdrom device could use this to read sensitive
    information from the kernel or to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17182">CVE-2018-17182</a>

    <p>Jann Horn discovered that the vmacache_flush_all function mishandles
    sequence number overflows. A local user can take advantage of this
    flaw to trigger a use-after-free, causing a denial of service
    (crash or memory corruption) or privilege escalation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.59-1.  This version also includes a fix for bug #898137 and
several other fixes included in upstream stable updates.</p>

<p>We recommend that you upgrade your linux packages.  Since the kernel
ABI and binary package names have changed, you will need to use an
upgrade command that installs new dependencies, such as <q>apt upgrade</q>
or "apt-get upgrade --with-new-pkgs".</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1529.data"
# $Id: $
