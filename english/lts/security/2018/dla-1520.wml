<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in the CPython interpreter which
can cause denial of service, information gain, and arbitrary code
execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000158">CVE-2017-1000158</a>

    <p>CPython (aka Python) is vulnerable to an integer overflow in the
    PyString_DecodeEscape function in stringobject.c, resulting in
    heap-based buffer overflow (and possible arbitrary code execution)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1060">CVE-2018-1060</a>

    <p>python is vulnerable to catastrophic backtracking in pop3lib's
    apop() method. An attacker could use this flaw to cause denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1061">CVE-2018-1061</a>

    <p>python is vulnerable to catastrophic backtracking in the
    difflib.IS_LINE_JUNK method. An attacker could use this flaw to
    cause denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000802">CVE-2018-1000802</a>

    <p>Python Software Foundation Python (CPython) version 2.7 contains a
    CWE-77: Improper Neutralization of Special Elements used in a
    Command ('Command Injection') vulnerability in shutil module
    (make_archive function) that can result in Denial of service,
    Information gain via injection of arbitrary files on the system or
    entire drive. This attack appear to be exploitable via Passage of
    unfiltered user input to the function.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.2-1+deb8u1.</p>

<p>We recommend that you upgrade your python3.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1520.data"
# $Id: $
