<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in isc-dhcp, a server for automatic IP address
assignment.</p>

<p>The number of simultaneous open TCP connections to OMAPI port of the
server has to be limited to 200 in order to avoid a denial of service.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.3.1-6+deb8u4.</p>

<p>We recommend that you upgrade your isc-dhcp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2003.data"
# $Id: $
