<define-tag pagetitle>Debian Project Leader Elections 2019</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />



# The Tags beginning with v are will become H3 headings and are defined in 
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms, 
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse, 
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss, 
# vballot, vforum, voutcome


    <vtimeline />
      <table class="vote">
          <tr>
            <th>Nomination period:</th> 
            <td>Sunday 2019-03-03 00:00:00 UTC</td>
            <td>Saturday 2019-03-16 23:59:59 UTC</td>
          </tr>
          <tr>
            <th>Campaigning period:</th>
            <td>Sunday 2019-03-17 00:00:00 UTC</td>
            <td>Saturday 2019-04-06 23:59:59 UTC</td>
          </tr>
          <tr>
            <th>Voting period:</th>
            <td>Sunday 2019-04-07 00:00:00 UTC</td>
            <td>Saturday 2019-04-20 23:59:59 UTC</td>
          </tr>
      </table>
#      <p>Please note that the new term for the project leader shall start on 2019-04-17.</p>
    
          <vnominations />
            <ol>
                <li>Joerg Jaspert [<email joerg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/03/msg00039.html'>nomination mail</a>] [<a href="platforms/joerg">platform</a>]
                <li>Jonathan Carter [<email jcc@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/03/msg00040.html'>nomination mail</a>] [<a href="platforms/jcc">platform</a>]
                <li>Sam Hartman [<email hartmans@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/03/msg00044.html'>nomination mail</a>] [<a href="platforms/hartmans">platform</a>]
                <li>Martin Michlmayr [<email tbm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/03/msg00054.html'>nomination mail</a>] [<a href="platforms/tbm">platform</a>]
                </li>
            </ol>

	  <vwithdrawn />
		<ul>
			<li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2019/03/msg00058.html'>nomination mail</a>]
				[<a href='https://lists.debian.org/debian-vote/2019/03/msg00191.html'>withdrawal mail</a>]
		</ul>


             <p>
                The ballot, when ready, can be requested through email
                by sending a signed email to
                <a href="mailto:ballot@vote.debian.org">ballot@vote.debian.org</a>
                with the subject leader2019.
             </p>

          <vstatistics />
          <p> 
            This year, like always, 
#		<a href="https://vote.debian.org/~secretary/leader2019/">statistics</a>
		<a href="suppl_001_stats">statistics</a>
		will be gathered about ballots received and
		acknowledgements sent periodically during the voting period.
		Additionally, the list of <a href="vote_001_voters.txt">voters</a> will be recorded.
		Also, the <a href="vote_001_tally.txt">tally
		sheet</a> will also be made available to be viewed.
		Please remember that the project leader election has a
		secret ballot, so the tally sheet will not contain the
		voter's name but a HMAC that allows the voters to check
		that their vote is in the list of votes.  There is a
		key generated for each voter that is send along with
		the ack for the vote.
         </p>


          <vquorum />
     <p>
        With the current list of <a href="vote_001_quorum.log">voting
          developers</a>, we have:
     </p>
    <pre>
#include 'vote_001_quorum.txt'
    </pre>
#include 'vote_001_quorum.src'


	  <vmajorityreq />
	  <p>The candidate needs a simple majority to be eligible.</p>
             
#include 'vote_001_majority.src'


          <voutcome />
#include 'vote_001_results.src'

    <hrline>
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>
