#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans OpenAFS,
un système de fichiers distribué.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16947">CVE-2018-16947</a>
<p>Le processus du contrôleur de sauvegarde sur bande accepte les RPC entrants
mais n'exige pas (ou n'autorise pas) l'authentification de ces RPC. La gestion
de ces RPC permet de réaliser des opérations avec les accréditations de
l'administrateur, incluant le vidage et la restauration du contenu des volumes
et la manipulation de la base de données de sauvegarde.</p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16948">CVE-2018-16948</a>

<p>Plusieurs routines du serveur RPC n'initialisent pas totalement les variables
de sortie avant de se terminer, divulguant des contenus de la mémoire à la fois
à partir de la pile et du tas. À cause du gestionnaire des fonctions de cache
d’OpenAFS comme serveur Rx pour le service AFSCB, les clients sont aussi
susceptible d’une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16949">CVE-2018-16949</a>

<p>Plusieurs types de données utilisées comme variable d’entrée RPC étaient
implémentés comme types de tableau illimité, limités seulement par le champ de
longueur inhérent à 32 bits à 4 GB. Un attaquant authentifié pourrait envoyer, ou
réclamer d’envoyer, des valeurs d’entrée élevées pour utiliser les ressources du
serveur attendant ces entrées, conduisant à un déni de service pour d’autres
utilisateurs.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.6.9-2+deb8u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openafs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1513.data"
# $Id: $
